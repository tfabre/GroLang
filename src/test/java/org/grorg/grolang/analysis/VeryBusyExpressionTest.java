package org.grorg.grolang.analysis;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.AstFactory;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.info.LabelGeneratorTest;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

final public class VeryBusyExpressionTest
{
    private static final BinaryArithmeticOperation aMinusB = new BinaryArithmeticOperation(
            BinaryArithmeticOperation.Operator.MINUS,
            new VariableName(new Identifier("a")),
            new VariableName(new Identifier("b")));
    private static final BinaryArithmeticOperation bMinusA = new BinaryArithmeticOperation(
            BinaryArithmeticOperation.Operator.MINUS,
            new VariableName(new Identifier("b")),
            new VariableName(new Identifier("a")));
    private VeryBusyExpression veryBusyExpression;
    private LabelGeneratorTest generator;

    @BeforeEach
    void init()
    {
        final String p = "program test begin int a, b, x, y;" +
                "if a > b then (" +
                "x := b - a; y := a - b) " +
                "else (" +
                "y := b - a; x := a -b)" +
                "end";
        GroLangLexer lexer = new GroLangLexer(CharStreams.fromString(p));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        GroLang parser = new GroLang(tokenStream);

        GroLang.ProgramContext ctx = parser.program();
        Program program = new AstFactory().visitProgram(ctx);
        Analyser analyser = new Analyser(program);
        this.veryBusyExpression = new VeryBusyExpression(analyser);
        this.generator = LabelGeneratorTest.newInstance();
    }

    @Test
    void veryBusyExpression()
    {
        final AnalysisResult<ArithmeticExpression<?>> result = this.veryBusyExpression.result();
        final Label L1 = this.generator.newLabel();
        final Label L2 = this.generator.newLabel();
        final Label L3 = this.generator.newLabel();
        final Label L4 = this.generator.newLabel();
        final Label L5 = this.generator.newLabel();

        assertEquals(HashMap.of(
                L1, HashSet.of(aMinusB, bMinusA),
                L2, HashSet.of(aMinusB, bMinusA),
                L3, HashSet.of(aMinusB),
                L4, HashSet.of(aMinusB, bMinusA),
                L5, HashSet.of(aMinusB)),
                result.entry());

        assertEquals(HashMap.of(
                L1, HashSet.of(aMinusB, bMinusA),
                L2, HashSet.of(aMinusB),
                L3, HashSet.empty(),
                L4, HashSet.of(aMinusB),
                L5, HashSet.empty()),
                result.exit());
    }
}
