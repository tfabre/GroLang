package org.grorg.grolang.analysis;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.AstFactory;
import org.grorg.grolang.ast.Identifier;
import org.grorg.grolang.ast.Program;
import org.grorg.grolang.ast.VariableName;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.info.LabelGeneratorTest;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.vavr.API.Tuple;
import static org.junit.jupiter.api.Assertions.assertEquals;

final class ReachableDefinitionTest
{
    private static final VariableName X = new VariableName(new Identifier("x"));
    private static final VariableName Y = new VariableName(new Identifier("y"));
    private ReachableDefinition reachableDefinition;
    private LabelGeneratorTest generator;
    private static final Label UNKNOWN = ReachableDefinition.UnknownLabel.INSTANCE;

    @BeforeEach
    void init()
    {
        final String p = "program test begin int x, y;" +
                "x := 5; y := 1;" +
                "while x > 1 do (y := x * y; x := x -1) " +
                "end";
        GroLangLexer lexer = new GroLangLexer(CharStreams.fromString(p));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        GroLang parser = new GroLang(tokenStream);

        GroLang.ProgramContext ctx = parser.program();
        Program program = new AstFactory().visitProgram(ctx);
        Analyser analyser = new Analyser(program);
        this.reachableDefinition = new ReachableDefinition(analyser);
        this.generator = LabelGeneratorTest.newInstance();
    }

    @Test
    void reachableDefinition()
    {
        final AnalysisResult<Tuple2<VariableName, Label>> result = this.reachableDefinition.result();
        final Label l1 = this.generator.newLabel();
        final Label l2 = this.generator.newLabel();
        final Label l3 = this.generator.newLabel();
        final Label l4 = this.generator.newLabel();
        final Label l5 = this.generator.newLabel();

        assertEquals(HashMap.of(
                l1, HashSet.of(Tuple(X, UNKNOWN), Tuple(Y, UNKNOWN)),
                l2, HashSet.of(Tuple(X, l1), Tuple(Y, UNKNOWN)),
                l3, HashSet.of(Tuple(X, l1), Tuple(Y, l2), Tuple(Y, l4), Tuple(X, l5)),
                l4, HashSet.of(Tuple(X, l1), Tuple(Y, l2), Tuple(Y, l4), Tuple(X, l5)),
                l5, HashSet.of(Tuple(X, l1), Tuple(Y, l4), Tuple(X, l5))),

                result.entry());

        assertEquals(HashMap.of(
                l1, HashSet.of(Tuple.of(X, l1), Tuple.of(Y, UNKNOWN)),
                l2, HashSet.of(Tuple.of(X, l1), Tuple.of(Y, l2)),
                l3, HashSet.of(Tuple.of(X, l1), Tuple.of(Y, l2), Tuple.of(Y, l4), Tuple.of(X, l5)),
                l4, HashSet.of(Tuple.of(X, l1), Tuple.of(Y, l4), Tuple.of(X, l5)),
                l5, HashSet.of(Tuple.of(Y, l4), Tuple.of(X, l5))),

                result.exit());
    }
}
