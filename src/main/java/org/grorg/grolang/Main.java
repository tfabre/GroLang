package org.grorg.grolang;

import io.vavr.control.Option;
import org.grorg.grolang.cli.Cli;

public final class Main
{
    public static void main(String... args)
    {
        Option<Cli> cli = Cli.parseFromCommandLine(args);
        if (!cli.isEmpty())
        {
            cli.get().process();
        }
    }
}
