package org.grorg.grolang.analysis;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Cons;
import static io.vavr.Predicates.instanceOf;

class Finals
{
    static Set<Label> finals(List<Statement> statements)
    {
        return Match(statements).of(
                Case($Cons($(), $(List.empty())), (x, xs) -> finals(x)),
                Case($Cons($(), $()), (x, xs) -> finals(xs))
        );
    }

    static Set<Label> finals(Statement statement)
    {
        return Match(statement).of(
                Case($(instanceOf(If.class)), Finals::finals),
                Case($(instanceOf(IfElse.class)), Finals::finals),
                Case($(instanceOf(ProcedureCall.class)), Finals::finals),
                Case($(), (stmt) -> HashSet.of(stmt.label()))
        );
    }

    private static Set<Label> finals(If ifStmt)
    {
        return finals(ifStmt.body());
    }

    private static Set<Label> finals(IfElse ifElse)
    {
        return finals(ifElse.ifBody()).union(finals(ifElse.elseBody()));
    }

    private static Set<Label> finals(ProcedureCall procedureCall)
    {
        return HashSet.of(procedureCall.exitLabel());
    }

    static Set<Label> finals(Procedure<?> procedure)
    {
        return HashSet.of(procedure.exitLabel());
    }
}
