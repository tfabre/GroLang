package org.grorg.grolang.analysis;

import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Cons;
import static io.vavr.Patterns.$Nil;
import static io.vavr.Predicates.instanceOf;

class Flows
{
    static Set<Tuple2<Label, Label>> flow(List<Statement> stmts)
    {
        return Match(stmts).of(
                Case($Nil(), HashSet::empty),
                Case($Cons($(), $(List.empty())), (x, xs) -> flow(x)),
                Case($Cons($(), $()), (x, xs) ->
                        flow(x)
                                .union(flow(xs))
                                .union(Finals.finals(x).map((l) -> Tuple(l, Inits.init(xs)))))
        );
    }

    private static Set<Tuple2<Label, Label>> flow(Statement stmt)
    {
        return Match(stmt).of(
                Case($(instanceOf(If.class)), Flows::flow),
                Case($(instanceOf(IfElse.class)), Flows::flow),
                Case($(instanceOf(While.class)), Flows::flow),
                Case($(instanceOf(ProcedureCall.class)), Flows::flow),
                Case($(), HashSet::empty)
        );
    }

    private static Set<Tuple2<Label, Label>> flow(If ifStmt)
    {
        return flow(ifStmt.body()).union(HashSet.of(Tuple(ifStmt.label(), Inits.init(ifStmt.body()))));
    }

    private static Set<Tuple2<Label, Label>> flow(IfElse ifElse)
    {
        return flow(ifElse.ifBody())
                .union(flow(ifElse.elseBody()))
                .union(HashSet.of(
                        Tuple(ifElse.label(), Inits.init(ifElse.ifBody())),
                        Tuple(ifElse.label(), Inits.init(ifElse.elseBody()))));
    }

    private static Set<Tuple2<Label, Label>> flow(While whileStmt)
    {
        return flow(whileStmt.body())
                .union(HashSet.of(Tuple(whileStmt.label(), Inits.init(whileStmt.body()))))
                .union(Finals.finals(whileStmt.body()).map(l -> Tuple(l, whileStmt.label())));
    }

    private static Set<Tuple2<Label, Label>> flow(ProcedureCall procedureCall)
    {
        // Only if the called procedure does not exist, this should not happen
        // In real world, this should be detected before this analysis!
        if (procedureCall.procedure().isEmpty())
        {
            return HashSet.empty();
        }
        Procedure<?> procedure = procedureCall.procedure().get();
        return HashSet.of(
                Tuple(procedureCall.entryLabel(), procedure.entryLabel()),
                Tuple(procedure.exitLabel(), procedureCall.exitLabel()));
    }

    static Set<Tuple2<Label, Label>> flow(Procedure<?> procedure)
    {
        return HashSet.of(Tuple(procedure.entryLabel(), Inits.init(procedure.body())))
                .union(flow(procedure.body()))
                .union(Finals.finals(procedure.body()).map(l -> Tuple(l, procedure.exitLabel())));
    }
}
