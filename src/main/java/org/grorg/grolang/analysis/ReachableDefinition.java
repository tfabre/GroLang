package org.grorg.grolang.analysis;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.AbstractSyntaxTree;
import org.grorg.grolang.ast.Assignment;
import org.grorg.grolang.ast.VariableName;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.*;
import static io.vavr.Patterns.$None;
import static io.vavr.Patterns.$Some;
import static io.vavr.Predicates.instanceOf;

public class ReachableDefinition extends Analysis<Tuple2<VariableName, Label>>
{
    enum  UnknownLabel implements Label
    {
        INSTANCE;

        @Override
        public String toString()
        {
            return "l?";
        }
    }

    private Analyser analyser;

    public ReachableDefinition(Analyser analyser)
    {
        this.analyser = analyser;
    }

    @Override
    boolean include(Set<Tuple2<VariableName, Label>> elem1, Set<Tuple2<VariableName, Label>> elem2)
    {
        return elem2.containsAll(elem1);
    }

    @Override
    Set<Tuple2<VariableName, Label>> union(Set<Tuple2<VariableName, Label>> elem1, Set<Tuple2<VariableName, Label>> elem2)
    {
        return elem1.union(elem2);
    }

    @Override
    Set<Tuple2<VariableName, Label>> defaultValue()
    {
        return HashSet.empty();
    }

    @Override
    Set<Tuple2<VariableName, Label>> firstValue()
    {
        return FreeVariable
                .freeVariable(this.analyser.program().body())
                .map(varName -> Tuple.of(varName, UnknownLabel.INSTANCE));
    }

    @Override
    Set<Label> entry()
    {
        return HashSet.of(analyser.init());
    }

    @Override
    Set<Tuple2<Label, Label>> flow()
    {
        return analyser.flow();
    }

    @Override
    Set<Tuple2<VariableName, Label>> kill(Label label)
    {
        return Match(analyser.blocks().get(label)).of(
                Case($Some($()), (AbstractSyntaxTree ast) -> Match(ast).of(
                        Case($(instanceOf(Assignment.class)),
                                (Assignment assignment) ->
                                        // all assignment to `assignment.affectedVariable()` in the program
                                        HashSet.ofAll(analyser.program().body()
                                                .filter(instanceOf(Assignment.class))
                                                .map(stmt -> (Assignment) stmt)
                                                .filter(a -> a.affectedVariable().equals(assignment.affectedVariable()))
                                                .map(a -> Tuple(a.affectedVariable(), a.label())))
                                                .add(Tuple(assignment.affectedVariable(), UnknownLabel.INSTANCE))),
                        Case($(), HashSet::empty))
                ),
                Case($None(), HashSet::empty));
    }

    @Override
    Set<Tuple2<VariableName, Label>> gen(Label label)
    {
        return Match(analyser.blocks().get(label)).of(
                Case($Some($()), (AbstractSyntaxTree ast) -> Match(ast).of(
                        Case($(instanceOf(Assignment.class)),
                                (Assignment assignment) ->
                                        HashSet.of(Tuple(assignment.affectedVariable(), assignment.label()))),
                        Case($(), HashSet::empty))
                ),
                Case($None(), HashSet::empty));
    }

    @Override
    public AnalysisResult<Tuple2<VariableName, Label>> result()
    {
        Map<Label, Set<Tuple2<VariableName, Label>>> result = runAnalysis();
        return new AnalysisResult<>("RD", result, mapTransferOnResult(result));
    }
}
