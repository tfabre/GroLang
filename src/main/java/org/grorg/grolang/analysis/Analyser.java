package org.grorg.grolang.analysis;

import io.vavr.Lazy;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.AbstractSyntaxTree;
import org.grorg.grolang.ast.ArithmeticExpression;
import org.grorg.grolang.ast.Program;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.Tuple;

public class Analyser
{
    private Program program;
    private Lazy<Label> init;
    private Lazy<Set<Label>> finals;
    private Lazy<Set<Label>> labels;
    private Lazy<Set<Tuple2<Label, Label>>> flow;
    private Lazy<Set<Tuple2<Label, Label>>> reverseFlow;
    private Lazy<Map<Label, AbstractSyntaxTree<?>>> blocks;
    private Lazy<Set<ArithmeticExpression<?>>> arithmeticExpressions;

    public Analyser(Program program)
    {
        this.program = program;
        this.init = Lazy.of(() -> Inits.init(program.body()));
        this.finals = Lazy.of(() -> Finals.finals(program.body()));
        this.labels = Lazy.of(() -> Labels.labels(program.body()).union(
                program.procedures()
                        .map(Labels::labels)
                        .foldLeft(HashSet.empty(), HashSet::union)));
        this.flow = Lazy.of(() -> Flows.flow(program.body()).union(
                program.procedures()
                        .map(Flows::flow)
                        .foldLeft(HashSet.empty(), HashSet::union)));
        this.reverseFlow = Lazy.of(() -> this.flow().map(l -> Tuple(l._2(), l._1())));
        this.blocks = Lazy.of(() -> Blocks.blocks(this.program.body())
                .merge(this.program.procedures().map(Blocks::blocks)
                        .foldLeft(Blocks.emptyMap(), (last, elem) -> last.merge(elem))));
        this.arithmeticExpressions = Lazy.of(() -> ArithmeticExpressions.arithmeticExpressions(program.body()));
    }

    public Program program()
    {
        return this.program;
    }

    public Label init()
    {
        return this.init.get();
    }

    public Set<Label> finals()
    {
        return this.finals.get();
    }

    public Set<Label> labels()
    {
        return this.labels.get();
    }

    public Set<Tuple2<Label, Label>> flow()
    {
        return this.flow.get();
    }

    public Set<Tuple2<Label, Label>> reverseFlow()
    {
        return this.reverseFlow.get();
    }

    public Map<Label, AbstractSyntaxTree<?>> blocks()
    {
        return blocks.get();
    }

    public Set<ArithmeticExpression<?>> arithmeticExpressions()
    {
        return this.arithmeticExpressions.get();
    }
}
