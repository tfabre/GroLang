package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

public abstract class Expression<T extends Expression<T>> extends AbstractSyntaxTree<T>
{
    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }
}
