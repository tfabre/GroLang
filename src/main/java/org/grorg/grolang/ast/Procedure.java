package org.grorg.grolang.ast;

import io.vavr.collection.List;
import org.grorg.grolang.ast.info.Label;

public abstract class Procedure<T extends Procedure<T>> extends AbstractSyntaxTree<T>
{
    private ProcedureBegin begin;
    private ProcedureEnd end;
    private Identifier name;
    private List<Variable> arguments;
    private List<Statement> body;

    public Procedure(Label entryLabel, Label exitLabel, Identifier name, List<Variable> arguments, List<Statement> body)
    {
        this.begin = new ProcedureBegin(entryLabel);
        this.end = new ProcedureEnd(exitLabel);
        this.name = name;
        this.arguments = arguments;
        this.body = body;
    }

    public ProcedureBegin begin()
    {
        return this.begin;
    }

    public ProcedureEnd end()
    {
        return this.end;
    }

    public Label entryLabel()
    {
        return begin.label();
    }

    public Label exitLabel()
    {
        return end.label();
    }

    public Identifier name()
    {
        return this.name;
    }

    public List<Variable> arguments()
    {
        return this.arguments;
    }

    public List<Statement> body()
    {
        return this.body;
    }
}
