package org.grorg.grolang.ast;

import io.vavr.collection.List;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class Function extends Procedure<Function>
{
    private Variable returnVar;

    public Function(Label entryLabel, Label exitLabel, Identifier name, List<Variable> arguments, List<Statement> body,
                    Variable returnVar)
    {
        super(entryLabel, exitLabel, name, arguments, body);
        this.returnVar = returnVar;
    }

    public Variable returnVar()
    {
        return returnVar;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected Function self()
    {
        return this;
    }
}
