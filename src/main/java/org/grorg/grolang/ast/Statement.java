package org.grorg.grolang.ast;

import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public abstract class Statement<T extends Statement<T>> extends AbstractSyntaxTree<T>
{
    public abstract Label label();

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }
}
