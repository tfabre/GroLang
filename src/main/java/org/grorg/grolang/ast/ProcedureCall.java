package org.grorg.grolang.ast;

import io.vavr.collection.List;
import io.vavr.control.Option;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class ProcedureCall extends Statement<ProcedureCall>
{
    private Label entryLabel;
    private Label exitLabel;
    private Identifier procedureName;
    private List<ArithmeticExpression> arguments;
    private Option<Procedure<?>> procedure = Option.none();

    public ProcedureCall(Label entryLabel, Label exitLabel, Identifier procedureName, List<ArithmeticExpression> arguments)
    {
        this.entryLabel = entryLabel;
        this.exitLabel = exitLabel;
        this.procedureName = procedureName;
        this.arguments = arguments;
    }

    @Override
    public Label label()
    {
        return this.exitLabel;
    }

    public Label entryLabel()
    {
        return this.entryLabel;
    }

    public Label exitLabel()
    {
        return this.exitLabel;
    }

    public Identifier procedureName()
    {
        return this.procedureName;
    }

    public List<ArithmeticExpression> arguments()
    {
        return this.arguments;
    }

    public Option<Procedure<?>> procedure()
    {
        return this.procedure;
    }

    public void setProcedure(Option<Procedure<?>> procedure)
    {
        this.procedure = procedure;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected ProcedureCall self()
    {
        return this;
    }
}
