package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

import java.util.Objects;

public final class VariableName extends ArithmeticExpression<VariableName>
{
    private Identifier name;

    public VariableName(Identifier name)
    {
        this.name = name;
    }

    public Identifier name()
    {
        return this.name;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected VariableName self()
    {
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        VariableName that = (VariableName) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name);
    }

    @Override
    public String toString()
    {
        return name.toString();
    }
}
