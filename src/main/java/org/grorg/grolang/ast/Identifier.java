package org.grorg.grolang.ast;

import java.util.Objects;

public final class Identifier
{
    private String identifier;

    public Identifier(String identifier)
    {
        this.identifier = identifier;
    }

    public String identifier()
    {
        return this.identifier;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Identifier that = (Identifier) o;
        return Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(identifier);
    }

    @Override
    public String toString()
    {
        return identifier;
    }
}
