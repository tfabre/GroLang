package org.grorg.grolang.ast;

import io.vavr.control.Option;
import org.grorg.grolang.ast.info.Info;
import org.grorg.grolang.ast.visitor.AstVisitor;

public abstract class AbstractSyntaxTree<T extends AbstractSyntaxTree<T>>
{
    private Option<Info> info = Option.none();

    public Option<Info> info()
    {
        return info;
    }

    public T setInfo(Info info)
    {
        this.info = Option.of(info);
        return self();
    }

    public abstract <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor);

    protected abstract T self();
}
