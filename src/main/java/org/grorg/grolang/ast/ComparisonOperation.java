package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

import java.util.Objects;

public final class ComparisonOperation extends BooleanExpression<ComparisonOperation>
{
    public enum Operator
    {
        LOWER_THAN,
        LOWER_THAN_OR_EQUAL,
        GREATER_THAN,
        GREATER_THAN_OR_EQUAL,
        EQUAL,
        DIFFERENT
    }
    private Operator operation;
    private ArithmeticExpression left;
    private ArithmeticExpression right;

    private ComparisonOperation(Operator operation, ArithmeticExpression left, ArithmeticExpression right)
    {
        this.operation = operation;
        this.left = left;
        this.right = right;
    }

    public Operator operation()
    {
        return operation;
    }

    public ArithmeticExpression left()
    {
        return left;
    }

    public ArithmeticExpression right()
    {
        return right;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected ComparisonOperation self()
    {
        return this;
    }

    public static ComparisonOperation lowerThan(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new ComparisonOperation(Operator.LOWER_THAN, left, right);
    }

    public static ComparisonOperation lowerThanOrEqual(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new ComparisonOperation(Operator.LOWER_THAN_OR_EQUAL, left, right);
    }

    public static ComparisonOperation greaterThan(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new ComparisonOperation(Operator.GREATER_THAN, left, right);
    }

    public static ComparisonOperation greaterThanOrEqual(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new ComparisonOperation(Operator.GREATER_THAN_OR_EQUAL, left, right);
    }

    public static ComparisonOperation equal(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new ComparisonOperation(Operator.EQUAL, left, right);
    }

    public static ComparisonOperation different(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new ComparisonOperation(Operator.DIFFERENT, left, right);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        ComparisonOperation that = (ComparisonOperation) o;
        return operation == that.operation &&
                // for == and != that are commutative
                ((operation == Operator.EQUAL || operation == Operator.DIFFERENT) &&
                        (Objects.equals(left, that.left) && Objects.equals(right, that.right)) ||
                        (Objects.equals(left, that.right) && Objects.equals(right, that.left)))
                ||
                // for others that aren't
                Objects.equals(left, that.left) &&
                Objects.equals(right, that.right);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(operation, left, right);
    }
}
