package org.grorg.grolang.ast;

import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class Skip extends Statement<Skip>
{
    private Label label;

    public Skip(Label label)
    {
        this.label = label;
    }

    @Override
    public Label label()
    {
        return this.label;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected Skip self()
    {
        return this;
    }
}
