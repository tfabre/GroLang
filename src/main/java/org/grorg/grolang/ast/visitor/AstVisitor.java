package org.grorg.grolang.ast.visitor;

import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Boolean;
import org.grorg.grolang.ast.Integer;

public interface AstVisitor<T>
{
    T visit(ArithmeticExpression expression);

    T visit(Assignment assignment);

    T visit(BinaryArithmeticOperation arithmeticOperation);

    T visit(Boolean booleanExpression);

    T visit(BooleanExpression expression);

    T visit(ComparisonOperation comparisonOperation);

    T visit(Consumer consumer);

    T visit(Expression expression);

    T visit(Function function);

    T visit(If ifStmt);

    T visit(IfElse ifElse);

    T visit(Integer integer);

    T visit(NotBooleanExpression expression);

    T visit(Procedure<? extends Procedure> procedure);

    T visit(ProcedureCall procedureCall);

    T visit(Program program);

    T visit(Skip skip);

    T visit(Statement stmt);

    T visit(UnaryArithmeticOperator unaryArithmeticOperator);

    T visit(Variable variable);

    T visit(VariableName variableName);

    T visit(While whileStmt);
}
