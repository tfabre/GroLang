package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

import java.util.Objects;

public final class NotBooleanExpression extends BooleanExpression<NotBooleanExpression>
{
    private BooleanExpression expression;

    public NotBooleanExpression(BooleanExpression expression)
    {
        this.expression = expression;
    }

    public BooleanExpression expression()
    {
        return this.expression;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected NotBooleanExpression self()
    {
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        NotBooleanExpression that = (NotBooleanExpression) o;
        return Objects.equals(expression, that.expression);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(expression);
    }
}
