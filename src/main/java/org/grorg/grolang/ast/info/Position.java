package org.grorg.grolang.ast.info;

public final class Position
{
    private int line;
    private int col;

    public Position(int line, int col)
    {
        this.line = line;
        this.col = col;
    }

    public int line()
    {
        return this.line;
    }

    public int col()
    {
        return this.col;
    }
}
