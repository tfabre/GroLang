package org.grorg.grolang.cli;


import io.vavr.collection.List;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.AstFactory;
import org.grorg.grolang.analysis.*;
import org.grorg.grolang.ast.Program;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static io.vavr.API.*;

public class Cli
{
    private enum AnalysisType
    {
        AVAILABLE_EXPRESSION,
        LIVING_VARIABLE,
        REACHABLE_DEFINITION,
        VERY_BUSY_EXPRESSION,
    }

    private static class CommandLineArguments
    {
        String inputFile = null;
        Option<String> outputFile = Option.none();
        List<AnalysisType> analysisTypes = List.empty();
    }

    private CommandLineArguments commandLineArguments;
    private Analyser analyser;

    private Cli(CommandLineArguments commandLineArguments, Program program)
    {
        this.commandLineArguments = commandLineArguments;
        this.analyser = new Analyser(program);
    }

    public void process()
    {
        List<AnalysisResult<?>> analysisResult = this.commandLineArguments.analysisTypes.map(type ->
            Match(type).of(
                    Case($(AnalysisType.AVAILABLE_EXPRESSION), new AvailableExpression(this.analyser)),
                    Case($(AnalysisType.LIVING_VARIABLE), new LivingVariable(this.analyser)),
                    Case($(AnalysisType.REACHABLE_DEFINITION), new ReachableDefinition(this.analyser)),
                    Case($(AnalysisType.VERY_BUSY_EXPRESSION), new VeryBusyExpression(this.analyser))
            ).result());


        this.commandLineArguments
                .outputFile
                .map(outputFile ->
                        Try.of(() -> new PrintStream(new FileOutputStream(outputFile))))
                .getOrElse(() -> Try.of(() -> System.out))
                .getOrElse(() -> System.out)
                .println(new RepresentationBuilder(analyser)
                        .generateDotWithAnalysis(analysisResult.toJavaList().toArray(new AnalysisResult[0])));
    }

    public static Option<Cli> parseFromCommandLine(String... args)
    {
        final CommandLineArguments arguments = new CommandLineArguments();

        for (int i = 0; i < args.length; ++i)
        {
            switch (args[i])
            {
                case "-o":
                case "--output":
                    if (++i < args.length)
                    {
                        arguments.outputFile = Option.some(args[i]);
                    }
                    break;
                case "-i":
                case "--input":
                    if (++i < args.length)
                    {
                        arguments.inputFile = args[i];
                    }
                    break;
                case "AE":
                case "AvailableExpression":
                    arguments.analysisTypes = arguments.analysisTypes.append(AnalysisType.AVAILABLE_EXPRESSION);
                    break;
                case "LV":
                case "LivingVariable":
                    arguments.analysisTypes = arguments.analysisTypes.append(AnalysisType.LIVING_VARIABLE);
                    break;
                case "RD":
                case "ReachableDefinition":
                    arguments.analysisTypes = arguments.analysisTypes.append(AnalysisType.REACHABLE_DEFINITION);
                    break;
                case "VBE":
                case "VeryBusyExpression":
                    arguments.analysisTypes = arguments.analysisTypes.append(AnalysisType.VERY_BUSY_EXPRESSION);
                    break;
            }
        }
        final Option<Program> program = checkArgumentsAndGetProgram(arguments);
        return program.map(p -> new Cli(arguments, p));
    }

    private static Option<Program> checkArgumentsAndGetProgram(CommandLineArguments arguments)
    {
        Option<Program> program = Option.none();
        if (arguments.inputFile == null || arguments.analysisTypes.equals(List.empty()))
        {
            displayHelp();
            return program;
        }
        final File inputFile = new File(arguments.inputFile);
        if (!inputFile.exists() || inputFile.isDirectory())
        {
            System.err.println("The input file '" + arguments.inputFile + "' not exists");
            return program;
        }

        try
        {
            GroLangLexer lexer = new GroLangLexer(CharStreams.fromFileName(arguments.inputFile));
            CommonTokenStream tokenStream = new CommonTokenStream(lexer);
            GroLang parser = new GroLang(tokenStream);

            if (parser.getNumberOfSyntaxErrors() > 0)
            {
                System.err.println("There is error on your program syntax.");
                return program;
            }

            GroLang.ProgramContext ctx = parser.program();
            program = Option.some(new AstFactory().visitProgram(ctx));
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
            return program;
        }

        return program;
    }

    private static void displayHelp()
    {
        System.out.println("GroLang - The analyser");
        System.out.println("Usage: java GroLang.jar <OPTION> <ANALYSIS>[ <ANALYSIS>]*");
        System.out.println();
        System.out.println("<OPTION>:");
        System.out.println("  -i, --input\tthe input program file to analyse [REQUIRED]");
        System.out.println("  -o, --output\tthe output file where we write analysis result [OPTIONAL]");
        System.out.println();
        System.out.println("<ANALYSIS>:");
        System.out.println("  AE, AvailableExpression\tthe available expression analysis");
        System.out.println("  LV, LivingVariable\tthe living variable analysis");
        System.out.println("  RD, ReachableDefinition\tthe reachable definition analysis");
        System.out.println("  VBE, VeryBusyExpression\tthe very busy expression analysis");
        System.out.println();
        System.out.println("This product is a GRORG software so that imply an high quality of service but");
        System.out.println("no warranty at all");

    }
}
